from django.urls import path
from hello_world import views

urlpatterns = [
    path('', views.hello_world, name='hello_world'),
    path('dictionaryMergeUpdateOperators', views.dictionaryMergeUpdateOperators, name='dictionaryMergeUpdateOperators'),
    path('stringRemovePrefixesAndSuffixes', views.stringRemovePrefixesAndSuffixes, name='stringRemovePrefixesAndSuffixes'),
    path('typeHintingGenericsInStandardCollections', views.typeHintingGenericsInStandardCollections, name='typeHintingGenericsInStandardCollections'),
    path('graphLib', views.graphLib, name='graphLib'),
]