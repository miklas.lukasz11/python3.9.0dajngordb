from django.shortcuts import render
import revdebug
from graphlib import TopologicalSorter
from datetime import date

def dictionaryMergeUpdateOperators(request):
    x = {"key1": "value1 from x", "key2": "value2 from x"}
    y = {"key2": "value2 from y", "key3": "value3 from y"}
    z = x | y
    print(z)
    revdebug.snapshot("dictionaryMergeUpdateOperators")
    return render(request, 'hello_world.html', {})

def stringRemovePrefixesAndSuffixes(request):
    str = "impossible"
    str= str.removeprefix("im")
    print(str)
    revdebug.snapshot("stringRemovePrefixesAndSuffixes")
    return render(request, 'hello_world.html', {})



def typeHintingGenericsInStandardCollections(request, names: list[str]) -> None:
    for name in names:
        print("Hello", name)
        revdebug.snapshot("typeHintingGenericsInStandardCollections")
        return render(request, 'hello_world.html', {})

def graphLib(request):
    graph = {"D": {"B", "C"}, "C": {"A"}, "B": {"A"}}
    ts = TopologicalSorter(graph)
    print(tuple(ts.static_order()))
    revdebug.snapshot("graphLib")
    return render(request, 'hello_world.html', {})


def hello_world(request):
    a=2/0
    return render(request, 'hello_world.html', {})

